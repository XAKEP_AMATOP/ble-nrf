#ifndef  _RBL_NRF8001_H
#define _RBL_NRF8001_H

#include "stm32l1xx.h"
#include "base.h"
#include "hal_platform.h"
#include "lib_aci.h"
#include "aci_setup.h"

/* Put the nRF8001 setup in the ROM of the nRF8001.*/
#include "services_lock.h"

extern FlagBase_t Flag;

extern CurrentData_t DataToSend;

//extern uint8_t updatingInterval;

/* typedef for protocol opcodes */
typedef enum
{
	START_TX = 0x81,//'s',    //0x81     // Change opcodes corresponding to protocol
	STOP_TX = 0x82, //'e',    //0x82
	CHANGE_INTERVAL = 0x80,//'c', //0x80
  CHANGE_TIME = 0x83//'t'  //0x83
} BT_PT_Opcodes;    // Bluetooth protocol opcodes


/* typedef for received data block*/
typedef struct
{
	char       startSymb;         // '$'
	uint8_t    op_code;           // Command for operation
  uint8_t    data[3];  // in seconds
	char       endSymb;	          // ';'
} BTLE_ReceivedData_t;

/*------ Functions prototypes ----------------------------- */

/**
  * @brief      This function used for configuration BTLE low level 
  *             of library and hardware interface
  * @return     No value
  */

void BTLE_begin(void);

/**
  * @brief      This function used for hardware reset of nRF8001 module
  * @note       te function takes about 62 ms 
  */

void BTLE_reset(void);

/**
  * @brief      This function used for sending data block via BLE UART TX Pipe 
  * @parameters data - pointer to data block; length - amount of data 
  *             for sending.
  * @return     Status:
  *       0     no error
  *       1     error
  */

uint8_t BTLE_UART_TX(uint8_t * data, uint8_t length);

/**
  * @brief      This function used for sending data block via BLE UART TX Pipe 
  * @parameters data - pointer to data block; length - amount of data 
  *             for sending.
  * @return     Status:
  *       0     no error
  *       1     error
  * @note       Max sending data size - 20 bytes
  */

uint8_t BTLE_SendData(uint8_t * data, uint8_t length, uint8_t isNewData);

/**
  * @brief      This function used for parsing received message and
  *             controlling BTLE behavior
  * @parameters msg - pointer on received message
  * @return     Status:
  *       0     no error
  *       1     error
  * @note       This function is internal library function. User 
  *             can't use it.  
  */

uint8_t ParseRxMsg(BTLE_ReceivedData_t * msg);

/**
  * @brief      This function is the main BTLE routing. 
  * @return     No value
  * @note       Call this function in infinit loop in main.c 
  */

void BTLE_process(void);



#endif /* _RBL_NRF8001_H */
