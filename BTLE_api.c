#include "BTLE_api.h"
#include "stm32l1xx.h"
#include <stdint.h>
#include <stdbool.h>

#define BTLE_UART_TX_MAX_DATA_SIZE       16U 

#ifdef SERVICES_PIPE_TYPE_MAPPING_CONTENT
    static services_pipe_type_mapping_t 
        services_pipe_type_mapping[NUMBER_OF_PIPES] = SERVICES_PIPE_TYPE_MAPPING_CONTENT;
#else
    #define NUMBER_OF_PIPES 0
    static services_pipe_type_mapping_t * services_pipe_type_mapping = 0;
#endif

/* Store the setup for the nRF8001 */ // Change it to stroe in Flash
static hal_aci_data_t setup_msgs[NB_SETUP_MESSAGES] = SETUP_MESSAGES_CONTENT;

static uint16_t Adv_Timeout = 180;	// in seconds
static uint16_t Adv_Interval = 0x0100; // advertising interval 50ms  /// before 50ms we are!!!

#define BTLE_MIN_CX_INTERVAL 0x0006 // * 1.25 ms = 7.5 ms
#define BTLE_MAX_CX_INTERVAL 0x0100//0x0012 // * 1.25 ms = 22.5 ms
#define BTLE_SLAVE_LATENCY 	 0x0000
#define BTLE_TIMEOUT         0x0c80 // * 10 ms = 32 000 ms

/** aci_struct that will contain :
  * total initial credits
  * current credit
  * current state of the aci (setup/standby/active/sleep)
  * open remote pipe pending
  * close remote pipe pending
  * Current pipe available bitmap
  * Current pipe closed bitmap
  * Current connection interval, slave latency and link supervision timeout
  * Current State of the the GATT client (Service Discovery)
  * Status of the bond (R) Peer address
  */

static aci_state_t aci_state;

/* Temporary buffers for sending ACI commands */
static hal_aci_evt_t  aci_data;


static BTLE_ReceivedData_t BTLE_ReceivedData;

extern uint8_t updatingInterval;

extern RTC_TimeTypeDef CurrentTime_Set;

/* Typedef for status register */
typedef struct
{
  uint8_t data_tx_allowed    : 1;
  uint8_t timing_change_done : 1;
} BTLE_status_t;

/* Structure included status flags and service variables */

static struct
{
  BTLE_status_t BTLE_status;
  uint8_t currentFrameNum;
} BTLE_Control;


/*-------------------------------------------------------------*/
/*-------------------------------------------------------------*/ 
/*-------------------------------------------------------------*/ 

void BTLE_begin()
{
 /**
  Point ACI data structures to the the setup data that the nRFgo studio generated for the nRF8001
  */ 
  if (NULL != services_pipe_type_mapping)
  {
    aci_state.aci_setup_info.services_pipe_type_mapping = &services_pipe_type_mapping[0];
  }
  else
  {
    aci_state.aci_setup_info.services_pipe_type_mapping = NULL;
  }
  aci_state.aci_setup_info.number_of_pipes    = NUMBER_OF_PIPES;
  aci_state.aci_setup_info.setup_msgs         = setup_msgs;
  aci_state.aci_setup_info.num_setup_msgs     = NB_SETUP_MESSAGES;
  
  BTLE_Control.BTLE_status.data_tx_allowed = 0;
  BTLE_Control.BTLE_status.timing_change_done = 0;
  BTLE_Control.currentFrameNum = 0;

  /** We reset the nRF8001 here by toggling the RESET line connected to the nRF8001
   *  and initialize the data structures required to setup the nRF8001
   */
  lib_aci_init(&aci_state);
}

/*-------------------------------------------------------------*/ 
/*-------------------------------------------------------------*/

void BTLE_reset(void)
{
	BTLE_RDYN_Irq(DISABLE);
  GPIO_WriteBit(BTLE_SPI_RST_PORT, BTLE_SPI_RST_PIN, Bit_RESET);
	GPIO_WriteBit(BTLE_SPI_RST_PORT, BTLE_SPI_RST_PIN, Bit_SET);
	delay(307600); // 307502 ~= 62 ms
  BTLE_RDYN_Irq(ENABLE);
}

/*-------------------------------------------------------------*/ 
/*-------------------------------------------------------------*/

uint8_t BTLE_UART_TX(uint8_t * data, uint8_t length)
{   
  if(PIPE_UART_OVER_BTLE_UART_TX_TX_MAX_SIZE < length)
  {
    return 1;                                     // Error
  }
  
  if(lib_aci_is_pipe_available(&aci_state, PIPE_UART_OVER_BTLE_UART_TX_TX))
  {
    if(BTLE_Control.BTLE_status.data_tx_allowed && aci_state.data_credit_available > 0)
    {
      lib_aci_send_data(PIPE_UART_OVER_BTLE_UART_TX_TX, data, length);
      aci_state.data_credit_available--;
			delay(200000);
      return 0;                                   // OK
    }
  }
  return 1;                                       // Error
}

/*-------------------------------------------------------------*/ 
/*-------------------------------------------------------------*/

uint8_t BTLE_SendData(uint8_t * data, uint8_t length, uint8_t isNewData) 
{
    static uint8_t * currentData;
    static uint8_t isRemainingData = 0;
    static uint8_t framesCnt = 0;
    static uint8_t lengthRemaining; 
    uint8_t t_length, status; 
    static uint8_t buffer[PIPE_UART_OVER_BTLE_UART_TX_TX_MAX_SIZE];   // Change it to malloc in the future
    
    if(!data || !length)
    {
        return 2;                                 // error - uncorrect input data
    }
    
    if(isRemainingData || isNewData)
    {
        if(!isRemainingData)
        {
            currentData = data;
            framesCnt = (length <= PIPE_UART_OVER_BTLE_UART_TX_TX_MAX_SIZE)? 1 :
                        (length / BTLE_UART_TX_MAX_DATA_SIZE + 1);
            lengthRemaining = length;
            status = 0;                              // All ok. Start to sending new data
        }
        else
        {
            currentData = (uint8_t*)((uint32_t)currentData + BTLE_UART_TX_MAX_DATA_SIZE);      // Shift data pointer to next part of data
            lengthRemaining -= BTLE_UART_TX_MAX_DATA_SIZE;
            status = 1;                              // We don't accept new data, we still send part of previos data
        }
        
        // Prepearing message to sending
        t_length = (lengthRemaining <= BTLE_UART_TX_MAX_DATA_SIZE)? lengthRemaining : BTLE_UART_TX_MAX_DATA_SIZE;    
        buffer[0] = '$';
        buffer[1] = framesCnt;
        buffer[2] = t_length;
        memcpy((buffer + 3), currentData, t_length);
        buffer[t_length + 3] = ';'; // t_length + 3 - offset containing control bytes plus data bytes
        
        // Send prepared message
        if(BTLE_UART_TX((uint8_t *)buffer, t_length + 4) != 0)
        {
            currentData = (uint8_t*)((uint32_t)currentData - BTLE_UART_TX_MAX_DATA_SIZE);      // Shift data pointer to next part of data
            lengthRemaining += BTLE_UART_TX_MAX_DATA_SIZE;
						return 3;                             // Error - current part of data wasn't sent
        }
        framesCnt--;
        
        isRemainingData = (framesCnt >= 1) ? 1 : 0;
				
     }
    return status;                               // If 0 - all ok. data sending started or doing nothing. If not 0 - some error occured
}

/*-------------------------------------------------------------*/ 
/*-------------------------------------------------------------*/

uint8_t ParseRxMsg(BTLE_ReceivedData_t * msg)
{
  if(msg->startSymb == '$' && msg->endSymb == ';')
  {
    switch(msg->op_code)
    {
      case START_TX:    // If received START_TX opcode  
        //BTLE_Control.BTLE_status.data_tx_allowed = 1;
							
				if(lib_aci_is_pipe_available(&aci_state, PIPE_UART_OVER_BTLE_UART_TX_TX))
				{
					Flag.flag_bt_synch = 1;
					BTLE_Control.BTLE_status.data_tx_allowed = 1;
				}
				else
				{
					Flag.flag_bt_synch = 0;
					BTLE_Control.BTLE_status.data_tx_allowed = 0;
				}
      break;
      
      case CHANGE_INTERVAL:
        updatingInterval = BTLE_ReceivedData.data[0];
      break;
      
      case STOP_TX:
				Flag.flag_bt_synch = 0;
      break;
      
      case CHANGE_TIME:
				CurrentTime_Set.RTC_H12 = RTC_HourFormat_24;
				CurrentTime_Set.RTC_Hours = BTLE_ReceivedData.data[0];
				CurrentTime_Set.RTC_Minutes = BTLE_ReceivedData.data[1];
				CurrentTime_Set.RTC_Seconds = BTLE_ReceivedData.data[2];
			
        Flag.flag_set_calibration_time = 1; 
        break;
      default : return 1;            // Error - undefined opcode
    }
    return 0;                        // OK
  }
  return 1;                          // OK
}

/*-------------------------------------------------------------*/ 
/*-------------------------------------------------------------*/

void BTLE_process(void)
{
	volatile uint8_t i;
	
	// We enter the if statement only when there is a ACI event available to be processed
  if (lib_aci_event_get(&aci_state, &aci_data))
  {
    aci_evt_t * aci_evt;
    
    aci_evt = &aci_data.evt;

    switch(aci_evt->evt_opcode)
    {
        /**
        As soon as you reset the nRF8001 you will get an ACI Device Started Event
        */
        case ACI_EVT_DEVICE_STARTED:
        {          
          aci_state.data_credit_total = aci_evt->params.device_started.credit_available;
         
					switch(aci_evt->params.device_started.device_mode)
          {
            case ACI_DEVICE_SETUP:
						{
							/**
							When the device is in the setup mode
							*/
							if (ACI_STATUS_TRANSACTION_COMPLETE != do_aci_setup(&aci_state))
							{
								 BTLE_reset();											// We had an error and handle it by hardware reseting nRF8001
							}
							break;
							
							case ACI_DEVICE_STANDBY:									// Evt Device Started: Standby
							{
								//Looking for a phone by sending radio advertisements
								//When an phone connects to us we will get an ACI_EVT_CONNECTED event from the nRF8001
							
								/* We Are !!!*/
							
								if (aci_evt->params.device_started.hw_error)
								{
										delay(20); //Magic number used to make sure the HW error event is handled correctly.
								}
								else
								{
										lib_aci_connect(Adv_Timeout/* in seconds */, Adv_Interval /* advertising interval 100ms*/);
								}
								break;
							}
						}
					}
				}
					
					
      case ACI_EVT_CMD_RSP:
			{
        //If an ACI command response event comes with an error -> stop
        if (ACI_STATUS_SUCCESS != aci_evt->params.cmd_rsp.cmd_status)
        {
          //ACI ReadDynamicData and ACI WriteDynamicData will have status codes of
          //TRANSACTION_CONTINUE and TRANSACTION_COMPLETE
          //all other ACI commands will have status code of ACI_STATUS_SCUCCESS for a successful command
            BTLE_reset();                       // Need testing...
        }
        if (ACI_CMD_GET_DEVICE_VERSION == aci_evt->params.cmd_rsp.cmd_opcode)
        {
          //Store the version and configuration information of the nRF8001 in the Hardware Revision String Characteristic
          lib_aci_set_local_data(&aci_state, PIPE_DEVICE_INFORMATION_HARDWARE_REVISION_STRING_SET, 
            (uint8_t *)&(aci_evt->params.cmd_rsp.params.get_device_version), sizeof(aci_evt_cmd_rsp_params_get_device_version_t));
        } 
			}
			break;
        
      case ACI_EVT_CONNECTED:
			{
        aci_state.data_credit_available = aci_state.data_credit_total;
			
				if(!aci_state.data_credit_available) break;  ///!!!! may be WRONG!!!!
        /*
        Get the device version of the nRF8001 and store it in the Hardware Revision String
        */
        lib_aci_device_version();
				Flag.flag_bt_conected = 1;
			}
      break;
        
      case ACI_EVT_PIPE_STATUS:
			{
        if (lib_aci_is_pipe_available(&aci_state, PIPE_UART_OVER_BTLE_UART_RX_RX) && (0 == BTLE_Control.BTLE_status.timing_change_done))
        {
          lib_aci_change_timing(BTLE_MIN_CX_INTERVAL, BTLE_MAX_CX_INTERVAL, BTLE_SLAVE_LATENCY, BTLE_TIMEOUT);
                                           
          BTLE_Control.BTLE_status.timing_change_done = 1;
        }
			}
			break;
       
      case ACI_EVT_DISCONNECTED:            // Disconnected/Advertising timed out
			{
       // BTLE_Control.BTLE_status.data_tx_allowed = 0;
				Flag.flag_bt_synch = 0;
				Flag.flag_bt_conected = 0;
        lib_aci_connect(180/* in seconds */, 0x0100 /* advertising interval 100ms*/);  

			}				
      break;                              // Advertising started
        
      case ACI_EVT_DATA_RECEIVED:
			{
        for(i=0; i<aci_evt->len - 2; i++)
				{
          *(((uint8_t*)&BTLE_ReceivedData)+i) = aci_evt->params.data_received.rx_data.aci_data[i];
				}
        ParseRxMsg(&BTLE_ReceivedData);
			}
			break;
			
      case ACI_EVT_DATA_CREDIT:
			{
        aci_state.data_credit_available = aci_state.data_credit_available + aci_evt->params.data_credit.credit;
				BTLE_Control.BTLE_status.data_tx_allowed = 1;
        BTLE_SendData((uint8_t*)&DataToSend, sizeof(DataToSend), 0/*no new data*/);  // Data sends only if it is necessary
			}
			break;
      
      case ACI_EVT_PIPE_ERROR:
			{
        //See the appendix in the nRF8001 Product Specication for details on the error codes
        //  aci_evt->params.pipe_error.pipe_number
			  //  aci_evt->params.pipe_error.error_code
				if(!lib_aci_is_pipe_available(&aci_state, PIPE_UART_OVER_BTLE_UART_TX_TX))
				{
					BTLE_Control.BTLE_status.data_tx_allowed = 0;
					Flag.flag_bt_synch = 0;   // Need testing
				}
                
        //Increment the credit available as the data packet was not sent
        aci_state.data_credit_available++;  ///// !!!!! CHECK ME!!!!
			}
      break;
				
			case ACI_EVT_HW_ERROR:	
			{
				__nop();
			
			}break;
			
			default :
			{
				__NOP();
			}
			break;
		}
	}
}

/*-------------------------------------------------------------*/ 
/*-------------------------------------------------------------*/
