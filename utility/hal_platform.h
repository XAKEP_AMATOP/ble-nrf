#ifndef PLATFORM_H__
#define PLATFORM_H__

#include "stm32l1xx.h"
#include "stm32l1xx_exti.h"
#include "stm32l1xx_syscfg.h"
#include "misc.h"
#include "m25p32.h"
#include <stdbool.h>
#include <string.h>

/* Ports and pins definitions */

#define BTLE_SPI                 SPI1
#define BTLE_SPI_RCC             RCC_APB2Periph_SPI1
#define RCC_APBxPeriphClockCmd   RCC_APB2PeriphClockCmd

/* Defines for MOSI pin */
#define BTLE_SPI_MOSI_PORT       GPIOA
#define BTLE_SPI_MOSI_PIN        GPIO_Pin_7 
#define BTLE_SPI_MOSI_AF         GPIO_AF_SPI1
#define BTLE_SPI_MOSI_RCC        RCC_AHBPeriph_GPIOA
#define BTLE_SPI_MOSI_SOURCE     GPIO_PinSource7

/* Defines for MISO pin*/
#define BTLE_SPI_MISO_PORT       GPIOB              // Moved from PA6 because it used by m25p32
#define BTLE_SPI_MISO_PIN        GPIO_Pin_4 
#define BTLE_SPI_MISO_AF         GPIO_AF_SPI1
#define BTLE_SPI_MISO_RCC        RCC_AHBPeriph_GPIOB
#define BTLE_SPI_MISO_SOURCE     GPIO_PinSource4

/* Defines for SCLK pin */
#define BTLE_SPI_SCLK_PORT       GPIOA
#define BTLE_SPI_SCLK_PIN        GPIO_Pin_5
#define BTLE_SPI_SCLK_AF         GPIO_AF_SPI1
#define BTLE_SPI_SCLK_RCC        RCC_AHBPeriph_GPIOA
#define BTLE_SPI_SCLK_SOURCE     GPIO_PinSource5

/* Defines for REQN pin */
#define BTLE_SPI_REQN_PORT       GPIOC
#define BTLE_SPI_REQN_PIN        GPIO_Pin_1
#define BTLE_SPI_REQN_RCC        RCC_AHBPeriph_GPIOC

/* Defines for RDYN pin*/
#define BTLE_SPI_RDYN_PORT       GPIOC
#define BTLE_SPI_RDYN_PIN        GPIO_Pin_2
#define BTLE_SPI_RDYN_RCC        RCC_AHBPeriph_GPIOC
#define BTLE_SPI_RDYN_EXTI_LINE  EXTI_Line2
#define BTLE_SPI_RDYN_EXTI_IRQn  EXTI2_IRQn
#define BTLE_SPI_RDYN_PORT_SRC   EXTI_PortSourceGPIOC
#define BTLE_SPI_RDYN_PIN_SRC    EXTI_PinSource2

/* Defines for RST pin */
#define BTLE_SPI_RST_PORT       GPIOC
#define BTLE_SPI_RST_PIN        GPIO_Pin_13
#define BTLE_SPI_RST_RCC        RCC_AHBPeriph_GPIOC

/* Definitions dependent on platform */

typedef enum {
	BTLE_MOSI,
	BTLE_MISO,
	BTLE_SCLK,
	BTLE_REQN,
	BTLE_RDYN,
	BTLE_RST
} BTLE_Pins_t;

void BTLE_spi_master_config(void);

void BTLE_clear_spi_master_config(void);

void BTLE_spi_master_reconfig(void);

void BTLE_IO_Config(BTLE_Pins_t io_name, uint8_t is_input);

void BTLE_IO_Set_State(BTLE_Pins_t io_name, BitAction io_state);

uint8_t BTLE_IO_Read(BTLE_Pins_t io_name);

void BTLE_RDYN_Irq(FunctionalState state);

void BTLE_USART_Write(char * str);

void sleepDisable(void);

void sleepEnable(void);
	
void delay(uint32_t t);

/* HAL definitions */

#define hal_pltf_spi_master_config() BTLE_spi_master_config()

#define hal_pltf_clear_spi_master_config() BTLE_clear_spi_master_config()

#define hal_pltf_enable_spi()  SPI_Cmd(BTLE_SPI, ENABLE)

#define hal_pltf_disable_spi() SPI_Cmd(BTLE_SPI, DISABLE)
  
#define hal_pltf_configure_spi_for_aci() do{\
  hal_pltf_spi_master_config();\
}while(0)


/**@brief Macro to set an output.
 *  @details
 *  This macro sets the given output to the given level (1 -> high, 0 -> low).
 *  @param io_name Output to change.
 *  @param io_state Level to set.
*/
#define HAL_IO_SET_STATE(io_name, io_state) BTLE_IO_Set_State(io_name, io_state)//digitalWrite(io_name, io_state) 


#endif /* PLATFORM_H__ */
