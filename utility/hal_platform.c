 #include "hal_platform.h"
 
void BTLE_spi_master_config(void)
{
  SPI_InitTypeDef SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStruct;
  
  /* SPI Periph clock enable */
  RCC_APBxPeriphClockCmd(BTLE_SPI_RCC, ENABLE);
	/* Enable clock for SYSCFG */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  /* Configure SPI Periph Clocks */
  RCC_AHBPeriphClockCmd(BTLE_SPI_REQN_RCC   | 
                        BTLE_SPI_MOSI_RCC | 
                        BTLE_SPI_MISO_RCC |
                        BTLE_SPI_SCLK_RCC, 
                        ENABLE);


  /* Configure the AF for MOSI, MISO and SCLK GPIO pins */
  GPIO_PinAFConfig(BTLE_SPI_MOSI_PORT, BTLE_SPI_MOSI_SOURCE, BTLE_SPI_MOSI_AF);
  GPIO_PinAFConfig(BTLE_SPI_MISO_PORT, BTLE_SPI_MISO_SOURCE, BTLE_SPI_MISO_AF);
  GPIO_PinAFConfig(BTLE_SPI_SCLK_PORT, BTLE_SPI_SCLK_SOURCE, BTLE_SPI_SCLK_AF);

  /* Configure interface pins: SCLK */
  GPIO_InitStructure.GPIO_Pin = BTLE_SPI_SCLK_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;    // External pull-down is used
  GPIO_Init(BTLE_SPI_SCLK_PORT, &GPIO_InitStructure);

  /* Configure interface pins: MISO */
  GPIO_InitStructure.GPIO_Pin = BTLE_SPI_MISO_PIN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
  GPIO_Init(BTLE_SPI_MISO_PORT, &GPIO_InitStructure);

  /* Configure interface pins: MOSI */
  GPIO_InitStructure.GPIO_Pin = BTLE_SPI_MOSI_PIN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;     // External pull-down is used
  GPIO_Init(BTLE_SPI_MOSI_PORT, &GPIO_InitStructure);

	/* Configure interface pins: REQN */
  GPIO_InitStructure.GPIO_Pin = BTLE_SPI_REQN_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(BTLE_SPI_REQN_PORT, &GPIO_InitStructure);
	
	GPIO_WriteBit(BTLE_SPI_REQN_PORT, BTLE_SPI_REQN_PIN, Bit_SET);
	
	/* Configure interface pins: RST */
  GPIO_InitStructure.GPIO_Pin = BTLE_SPI_RST_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(BTLE_SPI_RST_PORT, &GPIO_InitStructure);
  
  GPIO_WriteBit(BTLE_SPI_RST_PORT, BTLE_SPI_RST_PIN, Bit_SET);
	
	/* Configure interface pins: RDYN */
  GPIO_InitStructure.GPIO_Pin = BTLE_SPI_RDYN_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;     // May be wrong...
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(BTLE_SPI_REQN_PORT, &GPIO_InitStructure);
	
	/* Config external interrupt for RDYN lin */
	
	EXTI_InitStruct.EXTI_Line = BTLE_SPI_RDYN_EXTI_LINE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising_Falling; //EXTI_Trigger_Rising_Falling
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	
	SYSCFG_EXTILineConfig(BTLE_SPI_RDYN_PORT_SRC, BTLE_SPI_RDYN_PIN_SRC);
	
	EXTI_Init(&EXTI_InitStruct);
	
	NVIC_SetPriority(BTLE_SPI_RDYN_EXTI_IRQn, 2);
	
	//NVIC_EnableIRQ(BTLE_SPI_RDYN_EXTI_IRQn);
  
  /* SPI Config */
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;   // Operating frequency is 2 MHz
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_LSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(BTLE_SPI, &SPI_InitStructure);
  
  SPI_Cmd(BTLE_SPI, ENABLE); /*!< SPI enable */
}


void BTLE_clear_spi_master_config(void)
{
	while(SPI_I2S_GetFlagStatus(BTLE_SPI, SPI_I2S_FLAG_RXNE) == SET) {__NOP();}
  while(SPI_I2S_GetFlagStatus(BTLE_SPI, SPI_I2S_FLAG_TXE) == RESET) {__NOP();}
  while(SPI_I2S_GetFlagStatus(BTLE_SPI, SPI_I2S_FLAG_BSY) == SET) {__NOP();}	
	SPI_I2S_DeInit(BTLE_SPI);
}

void BTLE_spi_master_reconfig(void)
{
	SPI_InitTypeDef SPI_InitStructure; 
  
  /* SPI Config */
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;   // Operating frequency is 2 MHz
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_LSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  //SPI_Init(BTLE_SPI, &SPI_InitStructure);
  
  //SPI_Cmd(BTLE_SPI, ENABLE); /*!< SPI enable */
  
	while(SPI_I2S_GetFlagStatus(BTLE_SPI, SPI_I2S_FLAG_RXNE) == SET) {__NOP();}
  while(SPI_I2S_GetFlagStatus(BTLE_SPI, SPI_I2S_FLAG_TXE) == RESET) {__NOP();}
  while(SPI_I2S_GetFlagStatus(BTLE_SPI, SPI_I2S_FLAG_BSY) == SET) {__NOP();}
  
  SPI_Cmd(BTLE_SPI, DISABLE); /*!< SPI disable */
  SPI_Init(BTLE_SPI, &SPI_InitStructure);
  SPI_Cmd(BTLE_SPI, ENABLE); /*!< SPI enable */
}


void BTLE_IO_Set_State(BTLE_Pins_t io_name, BitAction io_state)
{
	switch (io_name)
	{
		case BTLE_MOSI : GPIO_WriteBit(BTLE_SPI_MOSI_PORT, BTLE_SPI_MOSI_PIN, io_state); break;
		//case BTLE_MISO : GPIO_WriteBit(BTLE_SPI_MISO_PORT, BTLE_SPI_MISO_PIN, io_state); break;
		case BTLE_SCLK : GPIO_WriteBit(BTLE_SPI_SCLK_PORT, BTLE_SPI_SCLK_PIN, io_state); break;
		case BTLE_REQN : GPIO_WriteBit(BTLE_SPI_REQN_PORT, BTLE_SPI_REQN_PIN, io_state); break;
		//case BTLE_RDYN : GPIO_WriteBit(BTLE_SPI_RDYN_PORT, BTLE_SPI_RDYN_PIN, io_state); break;
		case BTLE_RST  : GPIO_WriteBit(BTLE_SPI_RST_PORT, BTLE_SPI_RST_PIN, io_state); break;
		default : break;
	}
}

void BTLE_RDYN_Irq(FunctionalState state)
{	
	if(state == ENABLE)
	{
		NVIC_EnableIRQ(BTLE_SPI_RDYN_EXTI_IRQn);
	}
	else
	{
		NVIC_DisableIRQ(BTLE_SPI_RDYN_EXTI_IRQn);
	}
}


/* This function isn't currently in use
 */
void BTLE_USART_Write(char * str)
{

}

/* .... */
void delay( uint32_t t)
{
	while(t--) {__NOP();}
}

void sleepDisable(void)
{
	
}

void sleepEnable(void)
{
	
}
